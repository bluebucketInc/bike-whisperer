// lib imports
import React from "react";
import Spectrogram from "./components/_spectrogram";
import MyProvider, { MyContext } from "./components/_provider";

// Main Class that Renders Menu and Spectrogram Components
const SpectrogramViewer = () => (
  <MyProvider>
    <MyContext.Consumer>
      {context => <Spectrogram handleResize={context.handleResize} />}
    </MyContext.Consumer>
  </MyProvider>
);

// export
export default SpectrogramViewer;