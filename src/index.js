
// lib imports
import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './util/registerServiceWorker';

// styles
import 'semantic-ui-css/semantic.min.css';

// component
import App from './app';

ReactDOM.render(<App />, document.getElementById('root'));

// service worker
registerServiceWorker();