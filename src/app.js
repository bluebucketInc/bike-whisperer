
// lib imports
import React from "react";
import Spectrogram from "./spectrogram";
// styles
import "./styles/main.css";

// main app
const App = () => (
	<div className="App">
        <Spectrogram />
    </div>
);

// export
export default App;